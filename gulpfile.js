const gulp = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const sass = require('gulp-sass');
const watch = require('gulp-watch');
const del = require("del");

function compiler() {
    return gulp
        .src('./scss/style.scss')
        .pipe(sass())
        .pipe(gulp.dest('./css/'))
}

function sassWatcher() {
    gulp.watch('./scss/style.scss', gulp.series(compiler))
}

function css() {
    return gulp
        .src("scss/style.scss")
        .pipe(sass())
        .pipe(gulp.dest("./dist/styles/"))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest("./dist/styles/"));
}

function clean() {
    return del(["dist"]);
}

function html() {
    return (
        gulp
            .src(["index.html"])
            .pipe(gulp.dest("./dist/"))
    );
}

const watcher = gulp.series(sassWatcher);
const build = gulp.series(clean, gulp.parallel(css, html));

exports.html = html;
exports.css = css;
exports.clean = clean;
exports.watcher = watcher;
exports.build = build;